#!/bin/sh
# See LICENSE file for copyright and license details.

# Helper script to be ran once after the initial mass merge in
# order to populate the structure with the needed symlinks.

dryrun=""
[ "$1" = "-d" ] && dryrun="echo"

# make sure these correlate to lib/config.py
REPO_ROOT="${REPO_ROOT:-/srv/amprolla/merged}"

PAIRS="
	jessie                   1.0
	jessie                   oldoldstable
	jessie-backports         oldoldstable-backports
	jessie-proposed-updates  oldoldstable-proposed-updates
	jessie-security          oldoldstable-security
	jessie-updates           oldoldstable-updates

	ascii                    2.0
	ascii                    oldstable
	ascii-backports          oldstable-backports
	ascii-proposed-updates   oldstable-proposed-updates
	ascii-security           oldstable-security
	ascii-updates            oldstable-updates

	beowulf                  3.0
	beowulf                  stable
	beowulf-backports        stable-backports
	beowulf-proposed-updates stable-proposed-updates
	beowulf-security         stable-security
	beowulf-updates          stable-updates

	chimaera                  4.0
	chimaera                  testing
	chimaera-backports        testing-backports
	chimaera-proposed-updates testing-proposed-updates
	chimaera-security         testing-security
	chimaera-updates          testing-updates


	unstable                 ceres
"

$dryrun cd "$REPO_ROOT" || exit 1

echo "$PAIRS" | while read -r codename suite; do
	if [ -n "$codename" ] && [ -n "$suite" ]; then
		if echo "$codename" | grep -qv '^#'; then
			$dryrun ln -snfv "$codename" "$suite"
		fi
	fi
done
